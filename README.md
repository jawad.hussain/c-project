# C++ Project

## Initial Project Setup:
- Implimented the functions declared in student class and added missing get and set functions. It was added into `student.h`
- Called the `student.h` function from main file.
- Compiled it through **CMake**.
- Used `.gitignore` to make sure unneccessary file are not commited.
- To Enable compilation in either debug mode or release mode the following commands are used:
- **For Debug mode**: `cmake -DCMAKE_BUILD_TYPE=Debug ..`  
- **For Release mode**: `cmake -DCMAKE_BUILD_TYPE=Release ..`
  
## Separate the Declaration of Class from Implementation

- Seperated the implimentation of functions from header into `student.cpp`.
-  Created Static library of 'student.cpp'.
-  Created `CMakeLists.txt` for main and student,cpp.
-  Used Cmake to run all files.
  
## How to Run:

- Clone project using command `git clone https://gitlab.com/jawad.hussain/c-project.git`
- Nevigate to cloned folder and create file called build using `mkdir build`
- Run the following commands afterwards:
- `cmake ..`
- `make`
- `./app/main`