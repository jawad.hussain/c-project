cmake_minimum_required(VERSION 3.18)
project(cpp_project)
add_subdirectory(src)
add_subdirectory(app)
