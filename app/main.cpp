#include <iostream>
#include <iterator>
#include <map>
#include "student.h"
using namespace std;

int main()
{

    string roll_no;
    int age;
    float cgpa;
    cout << "\n---------- Setting Data ----------\n\n";
    cout << "Enter Roll no: ";
    cin >> roll_no;
    cout << "Enter age: ";
    cin >> age;
    cout << "Enter CGPA: ";
    cin >> cgpa;

    student obj1(roll_no, age, cgpa);

    cout << "\n---------- Printing Data ----------\n\n";
    cout << "roll_no: " << obj1.get_roll_no() << "\nage: " << obj1.get_age() << "\ncpga: " << obj1.get_cgpa() << endl;

    obj1.set_subject_marks("Math", 89);
    obj1.set_subject_marks("English", 22);
    obj1.set_subject_marks("Urdu", 99);
    obj1.set_subject_marks("Urdu", 95);
    obj1.print_all_marks();

    string test_subject[] = {"science", "Urdu"};

    cout << "\n---------- Printing specific data ----------\n\n";

    for (int i = 0; i < sizeof(test_subject) / sizeof(test_subject[0]); i++)
    {
        int marks = obj1.get_subject_marks(test_subject[i]);
        if (marks == -1)
        {
            cout << "Subject " << test_subject[i] << " does not exist!\n";
        }
        else
        {
            cout << "Subject " << test_subject[i] << ": \t" << marks << endl;
        }
    }
}