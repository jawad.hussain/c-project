// Contains all the Functions of student.h

#include "student.h"
using namespace std;

student::student()
{
    cout << "Constructor is called \n";
}

student::student(string roll_no, int age, float cgpa)
{
    cout << "\nConstructor is called \n\n";
    data.roll_no = roll_no;
    data.age = age;
    data.cgpa = cgpa;
};

student::~student()
{
    cout << "\n\nDestructor is called \n";
}

void student::set(string roll_no, int age, float cgpa)
{
    data.roll_no = roll_no;
    data.age = age;
    data.cgpa = cgpa;
};

string student::get_roll_no()
{
    return data.roll_no;
};

int student::get_age()
{
    return data.age;
};

float student::get_cgpa()
{
    return data.cgpa;
};

int student::get_subject_marks(std::string subject)
{
    map<string, int>::iterator it1;
    it1 = result.find(subject);
    if (it1 == result.end())
    {
        // -1 means subject not found
        return -1;
    }
    else
        return it1->second;
};

void student::set_subject_marks(std::string subject, int marks)
{
    result[subject] = marks;
    //    this->result.insert(pair<string, int>(subject, marks));
};

void student::print_all_marks()
{
    cout << "\n\n--------------- Dispay All Marks ---------------\n";
    map<string, int>::iterator it1;
    for (it1 = result.begin(); it1 != result.end(); ++it1)
    {
        cout << it1->first << ":    " << it1->second << "\n";
    }
};