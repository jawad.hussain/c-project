#include <iostream>
#include <iterator>
#include <map>
using namespace std;

class student
{
private:
    struct student_record
    {
        std::string roll_no;
        int age;
        float cgpa;
    };
    student_record data;
    std::map<std::string /*subject name*/, int /*marks*/> result;

public:
    student();
    student(string roll_no, int age, float cgpa);

    void set(string roll_no, int age, float cgpa);
    string get_roll_no();
    int get_age();
    float get_cgpa();
    int get_subject_marks(std::string subject);
    void set_subject_marks(std::string subject, int marks);
    void print_all_marks();
    ~student();
};